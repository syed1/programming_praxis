#include<stdio.h>
#include<string.h>
#include<stdlib.h>

/* We have another interview question today:
 
    Given a number, find the next higher number that uses the same set of digits. For instance, given the number 38276, the next higher number that uses the digits 2 3 6 7 8 is 38627.

Your task is to write a function to find the next greater permuation of digits */
/* for Qsort */

string_sort(char *str)
{
    int i,j;
    char tmp=127;
    for(i=0;i<strlen(str);i++)
    {
        for(j=0;j<i;j++)
        {
            if(str[i]<str[j])
            {
                tmp = str[i];
                str[i] = str[j];
                str[j] = tmp;
            }
        }
    }
    printf("sorted string %s\n",str);
}

int find_place(char *num)
{
    int i ;
    for( i =  strlen(num)-1;i>0 ; i--)
    {
        if ( num[i-1] < num[i] )
            return i-1;
    }
    return -1;
}

char * find_min(char *num)
{
    char last_num = num[0],new_num=127;
    int new_num_pos,i;

    printf("Find min %s\n",num);
    for(i=1;i<strlen(num);i++)
    {
        if ( (num[i] < new_num) && (num[i] > last_num ))
        {
            new_num = num[i];
            new_num_pos = i ;
        }
    }
    printf("new num:%c\n", new_num);
    num[new_num_pos] = num[0];
    //$qsort(num+1,strlen(num)-1,sizeof(char),NULL);
    string_sort(num+1);
    printf("%c",last_num);
    printf("%s\n" , num+1);
    num[0] = new_num;
    return num;
    
}
int main(int argc, char* argv[])
{
    char *number =  argv[1],*min;
    int place = 0,i; 

    /* My algo : 
     * Find the spot where we need to replace 
     * lets say numbr is 5873 , now start checking 
     * 1) 73 -> cant create a number greater than this with these numbers
     * 2) 873 -> same here 
     * 3) 5873 now here we can create a number greater as there are 
     * numbers in LSB which are greater than 5 , now we will choose 
     * a number which is least and greater than 5 7 in this case and 
     * place it in 5's place and we are left with 5,8,3 now we will create
     * the least number with them ie 358 so the final number is 7358 */

    place = find_place(number);
    if (place == -1)
    {
        printf("Not possible\n");
        return -1;
    }
    else 
    {
        printf("Place %d\n",place);
        min  = find_min(number+place);
    }
    printf("Finale \n");
    for(i=0;i<place;i++)
        printf("%c",number[i]);

    printf("%s",min);

}
    
