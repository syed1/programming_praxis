#include<stdio.h>
#include<string.h>


/*Balanced Delimiters
 March 2, 2012

These interview questions are addicting; I have to stop with them and get on something else. 
But one more won’t hurt:

    Write a function that takes a string and determines if the delimiters in the string are balanced. 
    The pairs of delimiters are (), [], {}, and , and delimiters may be nested. In addition, 
    determine that string delimiters ‘ and ” are properly matched; other delimiters 
    lose their magical delimiter-ness property within quoted strings. 
    Any delimiter is escaped if it follows a backslash. 
  
*/

#define IS_OPENING_PARAN(ch)  ( ch == '(' || ch == '[' || ch == '{' )
#define IS_CLOSING_PARAN(ch)  ( ch == ')' || ch == ']' || ch == '}' )

#define COMPARE_IF_PAIR(opening_paran,closing_paran) ( (opening_paran=='(')&&(closing_paran == ')')\
													 || (opening_paran=='{')&&(closing_paran == '}')\
													 || (opening_paran=='[')&&(closing_paran == ']') )


int main(int argc, char *argv[] )
{

    char stack[100];
    int sp=0,escape=0,in_string=0;
    int i;
    if (!argc)
        printf("Usage: ./a.out {()}\n");

    printf("Got %s\n",argv[1]);
    for( i = 0 ; i < strlen(argv[1]) ; i++ )
    {
        char ch = argv[1][i];
        if ( ch == '\'' || ch == '"' )
            in_string = !in_string;
        
        if(in_string)
            continue;

        if ( ch == '\\' )
            escape=1;
    
       
       if(IS_OPENING_PARAN(ch) && !escape )
           stack[sp++]=ch;
       else if( IS_CLOSING_PARAN(ch) && !escape)
       {
           char pop_val;
           pop_val = stack[--sp] ;
           printf("pop val %c",pop_val);
           if (! COMPARE_IF_PAIR(pop_val,ch) )
           {
               printf("Error unbalanced paranthesis\n");
               return -1;
           }
       }
        escape = 0 ;
    }
    if ( !sp )
        printf("Good parantheiss\n");
    else 
        printf("Bad paranthesis");

}
