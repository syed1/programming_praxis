#include<stdio.h>

/*
 We have an easy exercise for a Friday afternoon.

Consider the sequence of positive integers for which xn+1 = xn / 2 when x is even and 3·xn + 1 when xn is odd; this is known colloquially as “half or triple plus one.” For instance, starting from x0 = 13 the sequence is 13, 40, 20, 10, 5, 16, 8, 4, 2, 1, whence it loops through 4, 2, 1, …. This is called a hailstone sequence because it tends to go up and down and up and down much like hailstones in a thundercloud. Lothar Collatz conjectured in 1937 that every starting number eventually reaches 1; the conjecture is widely believed to be true, but has never been proven or disproven. */
int main(int argc, char *argv[])
{
    int num = atoi(argv[1]);
    printf("%d ," , num );
    while ( num != 1 )
        printf("%d ", (num&1)?(num = num*3+1):(num = num/2));
}
    
