/*A number n is a perfect power if there exists a b and e for which be = n. For
 * instance 216 = 63 = 23 · 33 is a perfect power, but 72 = 23 · 32 is not.
 * Testing for perfect powers is similar to other powering predicates we have
 * seen, and is useful in some factoring algorithms.
 
 * The trick to determining if a number is a perfect power is to know that, if
 * the number is a perfect power, then the exponent e must be less than log2 n,
 * because if e is greater then 2e will be greater than n. Further, it is only
 * necessary to test prime es, because if a number is a perfect power to a
 * composite exponent it will also be a perfect power to the prime factors of
 * the composite component; for instance, 215 = 32768 = 323 = 85 is a perfect
 * cube root and also a perfect fifth root. 

 * The trick here is to find the a combination of the base b and exponent e  for n
 * we will start with lowest base  2 and highest exponent  sqrt(n)  and 
 * will highest exponent (log2 n) and lowest base 2 and we try to converge 
 * increasing the base and exponent each time. There is a better solution 
 * using a prime sieve to bisect the interval and find the optimal exponent
 * but this would do for now */

#include<stdio.h>
#include<math.h>
#include<stdlib.h>


/* Function to find nth root of k by newtons method 
 * if x0 is an approximation to a function f, then x1 = x0 − f(x0) / f '(x0) is
 * a better approximation, where f ' is the derivative of f. 
 * f(x) = x^2 f'(x) = 2x 
 * f(x) = x^3 f'(x) = 3x^2 etc 
 * See also:  http://en.wikipedia.org/wiki/Nth_root_algorithm */

int introot(int k, int n )
{
    float root=n,oldroot=0;

    while( fabs(root - oldroot) > (0.001) ) // accuracy 
    {
        oldroot = root;
        root = (1.0/n)*((n-1)*root + (k/(pow(root,n-1))));
    }
    printf ("root %f oldroot %f \n",root,oldroot);
    return root;
}

/* Returns log n to the base e */
float logn(int  n , int b  ) 
{
    return (log2(n)/log2(b));
}

#define TRUE 1
#define FALSE 0

int main(int argc, char *argv[] )
{
    int minB,maxB,minE,maxE ;
    int num,is_perfect=0 ;
    num = atoi(argv[1]);
    if (argc != 2 ) 
    {
        printf("Usage: ./a.out <number to check for perfect power>\n");
        exit(-1);
    }

    minB = minE = 2 ;
    maxB = introot(num,minE);
    maxE =log2(minB);

    printf("num %d maxB %d , maxE %d\n",num,maxB,maxE);

    while(1) 
    {
        int rt1,rt2;
        rt1 = pow(maxB,minE);
        rt2 = pow(minB,maxE);
        if ( rt1 == num  || rt2 == num )
        {
            is_perfect = TRUE;
            break;
        }
        minE++;
        maxB = introot(num,minE);
        minB++;
        maxE=logn(num,minB);
        if ( minE > maxE )
        {
            is_perfect = FALSE;
            break;
        }
    }
    if ( is_perfect ) 
        printf("[TRUE]\n" , num );
    else 
        printf("[FALSE]\n" , num );
}
    
    
    

