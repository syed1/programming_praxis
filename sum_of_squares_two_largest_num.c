/* Today’s exercise comes to us from the book Structure and Interpretation of
 * Computer Programs by Abelson and Sussman (exercise 1.3):
 * 
 *     Define a procedure that takes three numbers as arguments and returns the
 *     sum of the squares of the two larger numbers.
 */

#include<stdio.h>
#include<stdlib.h>


#define GET_ARG_INT(i)  (atoi(argv[i+1]))

/* This also works (max(a,b)**2)+(max(min(a,b),c))**2 
 * finding min without brancing min(x, y) = y ^ ((x ^ y) & -(x < y));
 */

int sum_of_sq_largest_two(int a , int b , int c )
{
    if ( a < b && a < c )     
        return b*b + c*c ;
    if ( b < a && b < c )     
        return a*a + c*c;
    if ( c < b && c < a )     
        return a*a + b*b;
}

int main(int argc , char *argv[])
{
    if ( argc !=4 ) 
    {
        printf("Usage: ./a.out <num1> <num2> <num3>\n");
        exit(-1);
    }
    printf("Sum of sq of largest two %d " , sum_of_sq_largest_two(GET_ARG_INT(0),
                                                         GET_ARG_INT(1),GET_ARG_INT(2)));
}
    
